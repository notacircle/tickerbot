FROM node:10-alpine
WORKDIR /home/node/app
COPY ./package*.json ./
RUN npm install --only=prod
COPY . ./
EXPOSE 8443
CMD [ "node", "./src/app.js" ]
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
