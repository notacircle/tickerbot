const {
	NOMICS_API_KEY,
	MARKETCAP_TELEGRAM_TOKEN
} = process.env

const {
	TBFetchNomicsError,
	TBMessageFormatter
} = require('../../error')
const Worker = require('../worker')

const axios = require('axios')
const path = require('path')
const Sentry = require('@sentry/node')

class MarketcapWorker extends Worker {
	get config() {
		return {
			TELEGRAM_TOKEN: MARKETCAP_TELEGRAM_TOKEN,
			TICKER_UPDATE_INTERVAL: 60 * 1000, // 1 min
			WELCOME_MESSAGE: 'By pinning 📌 this bot you will always see the current Market Cap in the top of your chats..\n\n' +
				'If you have any problem (messages aren\'t updating) use /start command.',
			TIP_PICTURE: path.resolve(__dirname, '../../res/marketcap_tip.jpg'),
			ERROR_MESSAGE: '<a href="https://nomics.com">Nomics</a> our data provider is down. Please wait.',
			WORKER_NAME: 'marketcap',
			COMMUNITY_MESSAGE:
				'👨‍👩‍👧‍👦  <b>TickerBot Family:</b>\n\n' +
				'|   Bitcoin - @Bitcoin_TickerBot\n' +
				'|   Ethereum - @Ethereum_TickerBot\n' +
				'|   MarketCap - @MarketCap_TickerBot\n' +
				'|   Рубль - @Ruble_TickerBot\n\n' +
				'👥  <b>Add it to your <u>group</u> or <u>channel</u>!</b>\n\n' +
				' Join our <b>Community</b> 👇 if you need support.',
			JOIN_BUTTON_TEXT: '❤️ Join Community',
			UNSUPPORTED_CHAT_TYPE_MESSAGE:
				'⚠️ Unsupported chat type. This bot can be used in groups, ' +
				'channels or private chats',
			ONLY_ADMINS_CAN_START_MESSAGE:
				'⚠️ Only administrators can start this bot in groups.',
			CANT_BE_ADMIN_IN_CHANNELS_MESSAGE:
				'⚠️ This bot can\'t be an administrator in groups.\n\n' +
				'You must add it <b><u>as a regular member</u></b>.\n\n' +
				'Please note that only administrators can start.'
		}
	}

	async tickerMessage() {
		const _self = this

		const since = new Date()
		since.setDate(new Date().getDate()-2)
		const sinceISOString = since.toISOString()
		const url = `https://api.nomics.com/v1/market-cap/history?key=${NOMICS_API_KEY}&start=${sinceISOString}`
		try {
			const { data: responseData } = await axios.get(url)

			const dayAgoMarketcap = responseData[responseData.length-25]['market_cap']
			const currentMarketcap = responseData[responseData.length-1]['market_cap']

			const marketCapBil = currentMarketcap / (1000*1000*1000)
			const change24h = (currentMarketcap-dayAgoMarketcap)/(Math.abs(dayAgoMarketcap)/100)

			return `${ (change24h >= 0) ? '👍' : '👎' } $${marketCapBil.toFixed(2)} Bil ` +
				`(${ change24h > 0 ? '+' : '' }${change24h.toFixed(2)}%)`
		} catch (e) {
			Sentry.captureException(
				new TBFetchNomicsError(TBMessageFormatter([
					'Error fetching MarketCap Nomics data.',
					TBMessageFormatter({ 'Url': url })
				]), e))
			return _self.config.ERROR_MESSAGE
		}
	}
}

module.exports = MarketcapWorker
