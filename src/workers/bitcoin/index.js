const {
	NOMICS_API_KEY,
	BITCOIN_TELEGRAM_TOKEN
} = process.env

const {
	TBFetchNomicsError,
	TBMessageFormatter
} = require('../../error')
const Worker = require('../worker')

const axios = require('axios')
const path = require('path')
const Sentry = require('@sentry/node')

class BitcoinWorker extends Worker {
	get config() {
		return {
			TELEGRAM_TOKEN: BITCOIN_TELEGRAM_TOKEN,
			TICKER_UPDATE_INTERVAL: 60 * 1000, // 1 min
			WELCOME_MESSAGE:
				'By pinning 📌 this bot you will always see the current Bitcoin price in the top of your chats.\n\n' +
				'If you have any problem (messages aren\'t updating) use /start command.',
			TIP_PICTURE: path.resolve(__dirname, '../../res/bitcoin_tip.jpg'),
			ERROR_MESSAGE: '<a href="https://nomics.com">Nomics</a> our data provider is down. Please wait.',
			WORKER_NAME: 'bitcoin',
			COMMUNITY_MESSAGE:
				'👨‍👩‍👧‍👦  <b>TickerBot Family:</b>\n\n' +
				'|   Bitcoin - @Bitcoin_TickerBot\n' +
				'|   Ethereum - @Ethereum_TickerBot\n' +
				'|   MarketCap - @MarketCap_TickerBot\n' +
				'|   Рубль - @Ruble_TickerBot\n\n' +
				'👥  <b>Add it to your <u>group</u> or <u>channel</u>!</b>\n\n' +
				' Join our <b>Community</b> 👇 if you need support.',
			JOIN_BUTTON_TEXT: '❤️ Join Community',
			UNSUPPORTED_CHAT_TYPE_MESSAGE:
				'⚠️ Unsupported chat type. This bot can be used in groups, ' +
				'channels or private chats',
			ONLY_ADMINS_CAN_START_MESSAGE:
				'⚠️ Only administrators can start this bot in groups.',
			CANT_BE_ADMIN_IN_CHANNELS_MESSAGE:
				'⚠️ This bot can\'t be an administrator in groups.\n\n' +
				'You must add it <b><u>as a regular member</u></b>.\n\n' +
				'Please note that only administrators can start.'
		}
	}

	async tickerMessage() {
		const _self = this

		const url = `https://api.nomics.com/v1/currencies/ticker?key=${NOMICS_API_KEY}&ids=BTC&interval=1d`
		try {
			const { data: responseData } = await axios.get(url)
			const price = parseFloat(responseData[0]['price'])
			const change24h = parseFloat(responseData[0]['1d']['price_change_pct'])*100

			return `${ (change24h >= 0) ? '👍' : '👎' } $${price.toFixed(2)} ` +
				`(${ change24h > 0 ? '+' : '' }${change24h.toFixed(2)}%)`
		} catch (e) {
			Sentry.captureException(
				new TBFetchNomicsError(TBMessageFormatter([
					'Error fetching Bitcoin Nomics data.',
					TBMessageFormatter({ 'Url': url })
				]), e))
			return _self.config.ERROR_MESSAGE
		}
	}
}

module.exports = BitcoinWorker
