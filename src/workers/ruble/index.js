const {
	RUBLE_TELEGRAM_TOKEN
} = process.env

const {
	TBFetchCbrError,
	TBFetchMoexError,
	TBMessageFormatter
} = require('../../error')
const Worker = require('../worker')

const axios = require('axios')
const path = require('path')
const Micex = require('micex.api')
const Sentry = require('@sentry/node')

class RubleWorker extends Worker {
	get config() {
		return {
			TELEGRAM_TOKEN: RUBLE_TELEGRAM_TOKEN,
			TICKER_UPDATE_INTERVAL: 60 * 1000, // 1 min
			WELCOME_MESSAGE: 'Закрепив этот бот 📌 вы всегда будете видеть курс рубля по данным ММВБ / ЦБ РФ вверху ваших чатов.\n\n' +
				'Если курс не обновляется повторите команду /start.',
			TIP_PICTURE: path.resolve(__dirname, '../../res/ruble_tip.jpg'),
			ERROR_MESSAGE: 'Источник данных (ММВБ / ЦБ РФ) недоступен. Подождите.',
			WORKER_NAME: 'ruble',
			COMMUNITY_MESSAGE:
				'👨‍👩‍👧‍👦 <b>Все боты TickerBot:</b>\n\n' +
				'|   Bitcoin - @Bitcoin_TickerBot\n' +
				'|   Ethereum - @Ethereum_TickerBot\n' +
				'|   MarketCap - @MarketCap_TickerBot\n' +
				'|   Рубль - @Ruble_TickerBot\n\n' +
				'👥  <b>Добавляйте в <u>группу</u> или <u>канал</u>!</b>\n\n' +
				' Вступайте в нашу 👇 <b>группу</b>.',
			JOIN_BUTTON_TEXT: '❤️  TickerBot Talk',
			UNSUPPORTED_CHAT_TYPE_MESSAGE:
				'⚠️ Неподдерживаемый тип чата. Бот работает только в ' +
				'группах, каналах и приватных чатах.',
			ONLY_ADMINS_CAN_START_MESSAGE:
				'⚠️ Только администраторы могут запускать бот в группах.',
			CANT_BE_ADMIN_IN_CHANNELS_MESSAGE:
				'⚠️ Бот не может быть администратором в группах.\n\n' +
				'Добавьте его <b><u>как обычного пользователя</u></b>.\n\n' +
				'Бот смогут запустить только администраторы группы.'
		}
	}

	async tickerMessage() {
		const _self = this
		// Fetch CBR data
		const cbrUrl = 'https://www.cbr-xml-daily.ru/daily_eng_utf8.xml'
		let rubleCbr = null
		try {
			rubleCbr =
				parseFloat(
					(await axios.get(cbrUrl)).data
						.match(/<Name>US Dollar<\/Name><Value>([\d,]*)<\/Value>/s)[1]
						.replace(',', '.')
				)
		} catch(e) {
			Sentry.captureException(
				new TBFetchCbrError(TBMessageFormatter([
					'Error fetching Ruble CBR data.',
					TBMessageFormatter({ 'Url': cbrUrl })
				]), e))
		}

		// Fetch MOEX data
		const moexTickerId = 'USD000UTSTOM'
		let rubleMoex = null
		try {
			rubleMoex = (await Micex['securityMarketdata'](moexTickerId)).node.last
		} catch(e) {
			Sentry.captureException(
				new TBFetchMoexError(TBMessageFormatter([
					'Error fetching Ruble MOEX data.',
					TBMessageFormatter({ 'MOEX Ticker ID': moexTickerId })
				]), e))
		}

		return (rubleCbr && rubleMoex) ?
			`ММВБ: ${rubleMoex}; ЦБ: ${rubleCbr} руб. / USD`
			: _self.config.ERROR_MESSAGE
	}
}

module.exports = RubleWorker
