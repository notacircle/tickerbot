module.exports = [
	require('./bitcoin'),
	require('./ethereum'),
	require('./marketcap'),
	require('./ruble')
]
