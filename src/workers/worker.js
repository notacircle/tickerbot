const {
	ENVIRONMENT,
	SERVER_ADDRESS,
	WEBHOOK_SERVER_PORT,
	MIXPANEL_TOKEN
} = process.env

const error = require('../error')
const {
	TBMessageFormatter,
	TBMessageCannotBeEditedError,
	TBTelegrafInternalError,
	TBResolveServerIpError
} = error
const { User } = require('../models')

const _debug = require('debug')('worker')
const path = require('path')
const crypto = require('crypto')
const Sentry = require('@sentry/node')
const Telegraf = require('telegraf')
const Markup = require('telegraf/markup')
const Telegram = require('telegraf/telegram')
const TelegrafMixpanel = require('telegraf-mixpanel')
const { IpFilter, IpDeniedError } = require('express-ipfilter')
const dnsSync = require('dns-sync')

class Worker {
	constructor(webhookServer = null) {
		const _self = this

		// Pass webhookServer argument into this.
		_self._webhookServer = webhookServer

		if(_self.config == null) {
			_debug('You need to override config() method of Worker class.')
			return
		}

		// Define custom debug function for
		// each worker depending on his name.
		_self.debugWorker = _debug.extend(_self.config.WORKER_NAME)

		// Worker state
		_self.state = {
			cachedMessage: '',
			launched: false
		}

		// Telegram native API.
		_self._telegram = new Telegram(_self.config.TELEGRAM_TOKEN)

		// Telegraf instance initialize.
		_self._telegraf = new Telegraf(_self.config.TELEGRAM_TOKEN,
			{ telegram: { webhookReply: false } })
	}

	get config() {
		return null
	}

	async tickerMessage() {
		return ''
	}

	async updateTickers() {
		const _self = this
		const debug = _self.debugWorker.extend('update')

		const message = await _self.tickerMessage()

		// Update only if cached message
		// doesn`t equal the actual.
		if(message !== _self.state.cachedMessage) {
			_self.state.cachedMessage = message

			debug('Update tickers.')
			const users = await User.find({
				workerName: _self.config.WORKER_NAME
			})

			debug(`Tickers found: ${users.length}.`)
			for(const { chatId, tickerMessageId, chatType } of users) {
				// Skip user with undefined or null tickerMessageId.
				if(!tickerMessageId) {
					debug(`Skipping for ${chatId}.`)
					continue
				}

				debug(`Update for ${chatId}.`)
				try {
					await _self._telegram.editMessageText(chatId, tickerMessageId,
						null, message, { parse_mode: 'HTML' })
				} catch (e) {
					// We have a big zoo of errors when updating Telegram messages,
					// so it's much better to catch and ignore them.

					// Local formatter.
					// eslint-disable-next-line no-inner-declarations
					function formatError(message) {
						return TBMessageFormatter([
							message,
							TBMessageFormatter({
								chatId, chatType, tickerMessageId,
								workerName: _self.config.WORKER_NAME,
							})
						])
					}

					switch(e.message) {
					case error.known.MESSAGE_IS_NOT_MODIFIED:
						// Occurs when updated message is the same.
						// It's better to ignore and don't prevent.
						break
					case error.known.MESSAGE_TO_EDIT_NOT_FOUND:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'Message is deleted (tickerMessageId is reset).'))
						break
					case error.known.BOT_IS_NOT_A_MEMBER_OF_SUPERGROUP:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'Bot is kicked from supergroup (tickerMessageId is reset).'))
						break
					case error.known.BOT_WAS_KICKED_FROM_THE_GROUP_CHAT:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'Bot is kicked from the common group chat (tickerMessageId is reset).'))
						break
					case error.known.CHAT_NOT_FOUND:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'User was removed chat (tickerMessageId is reset).'))
						break
					case error.known.MESSAGE_IS_INVALID:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'User was removed message from channel (tickerMessageId is reset).'))
						break
					case error.known.BOT_IS_NOT_A_MEMBER_OF_CHANNEL:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'User was removed bot from channel (tickerMessageId is reset).'))
						break
					case error.known.MESSAGE_CANT_BE_EDITED:
						await User.resetTickerMessageId(
							{ workerName: _self.config.WORKER_NAME, chatId })
						Sentry.captureMessage(formatError(
							'Message can\'t be edited, probably user has restricted ' +
							'bot rights (tickerMessageId is reset).'))
						break
					default:
						Sentry.captureException(new TBMessageCannotBeEditedError(
							formatError('Unhandled tickerMessageId update error!'), e))
					}
				}
			}
		} else debug('Cached message is actual.')
	}

	async _start(ctx) {
		const _self = this
		const debug = _self.debugWorker.extend('start')

		const { id: chatId, username: username, first_name: firstName,
			last_name: lastName, type: chatType, title: title } = await ctx.getChat()

		debug(`Running /start command in ${chatType} by ${chatId}.`)

		if(chatType !== 'private'
			&& chatType !== 'group'
			&& chatType !== 'supergroup'
			&& chatType !== 'channel') {
			return ctx.reply(_self.config.UNSUPPORTED_CHAT_TYPE_MESSAGE)
		}

		if(chatType === 'group'
			|| chatType === 'supergroup') {
			const userId = ctx.from.id
			const admins = await ctx.getChatAdministrators()
			if(!admins.find((admin) => admin.user.id === userId)) {
				return ctx.reply(_self.config.ONLY_ADMINS_CAN_START_MESSAGE)
			}

			const { id: botId } = ctx.botInfo
			if(admins.find((admin) => admin.user.id === botId)) {
				return ctx.reply(_self.config.CANT_BE_ADMIN_IN_CHANNELS_MESSAGE,
					{ parse_mode: 'HTML' })
			}
		}

		// ******
		// Sending messages
		// ******
		// Send tip picture
		debug(`Sending tip picture to ${chatId}.`)
		await ctx.replyWithPhoto(
			{ source: _self.config.TIP_PICTURE },
			{ caption: _self.config.WELCOME_MESSAGE, parse_mode: 'Markdown' })

		// Send community message
		debug(`Sending community message to ${chatId}.`)
		if(_self.config.COMMUNITY_MESSAGE && _self.config.JOIN_BUTTON_TEXT) {
			await ctx.replyWithHTML(_self.config.COMMUNITY_MESSAGE,
				Markup.inlineKeyboard([
					Markup.urlButton(
						_self.config.JOIN_BUTTON_TEXT, 'http://t.me/TickerBot_Talk')
				]).extra())
		}

		// Send ticker message
		debug(`Sending ticker message to ${chatId}.`)
		const { message_id: tickerMessageId } =
			await ctx.reply(_self.state.cachedMessage, { parse_mode: 'HTML' })

		// ******
		// Database update
		// ******
		// Update user data
		debug(`Update user data in database for ${chatId}.`)
		const { isNew: isNewUser } = await User.findOneAndUpsert(
			{ workerName: _self.config.WORKER_NAME, chatId },
			{ tickerMessageId, chatType, username, firstName, lastName, title })

		// ******
		// User tracking
		// ******
		// Detect user traffic source (?start=%s) for private chats
		let source = `?${chatType}`
		if(chatType === 'private') {
			if(Array.isArray(ctx.match) && ctx.match[1]) {
				source = ctx.match[1]
			} else source = '?unknown'
		}

		// Update user's data in MixPanel
		if(chatType === 'private') {
			await ctx.mixpanel.people.set(Object.assign(
				isNewUser ?
					{ $created: new Date().toISOString(), chatId, source }
					: {},
				{
					modified: new Date().toISOString()
				}))
		}

		// Send user track with traffic source data
		await ctx.mixpanel.track(
			isNewUser ? 'start' : 'restart',
			{ worker: _self.config.WORKER_NAME, chatId, chatType, source })
	}

	async _stop(ctx) {
		const _self = this
		const debug = _self.debugWorker.extend('start')

		const { id: chatId, type: chatType } = await ctx.getChat()

		debug(`Running /stop command in ${chatType} by ${chatId}.`)

		// Stop getting updates for user
		await User.resetTickerMessageId(
			{ workerName: _self.config.WORKER_NAME, chatId })

		// Send stop tracking
		await ctx.mixpanel.track('stop',
			{ worker: _self.config.WORKER_NAME, chatId, chatType })
	}



	async launch() {
		const _self = this
		const debug = _self.debugWorker.extend('launch')

		if(_self.state.launched === false) {
			_self.state.launched = true

			debug('Launch worker.')

			// Setup MixPanel middleware
			_self._telegraf.use(
				(new TelegrafMixpanel(MIXPANEL_TOKEN)).middleware())

			// Setup middleware for cached
			// telegram.getMe()
			const _botInfo = await _self._telegraf.telegram.getMe()
			_self._telegraf.use(
				async (ctx, next) => {
					ctx.botInfo = _botInfo
					return next()
				})

			// Hears for '/start' command
			// in channels
			_self._telegraf.on('channel_post',
				async (ctx, next) => {
					const postText = ctx.channelPost.text
					if(postText === '/start'
						|| postText === `/start@${ctx.botInfo.username}`) {
						return _self._start(ctx)
					}
					return next()
				})

			// Hears for '/start' command in groups,
			// supergroups and private
			_self._telegraf.hears([
				'/start',
				`/start@${_botInfo.username}`,
				new RegExp('^/start (.*)$')],
			(ctx) => {
				return _self._start(ctx)
			})

			// Executes on '/stop' command
			_self._telegraf.command('stop', _self._stop.bind(_self))

			// Catching all errors thrown in
			// telegraf middlewares
			_self._telegraf.catch(async (e, ctx) => {
				const { id: chatId, type: chatType } = await ctx.getChat()

				// Local formatter
				function formatError(message) {
					return TBMessageFormatter([
						message,
						TBMessageFormatter({
							chatId,
							workerName: _self.config.WORKER_NAME,
							chatType
						})])
				}

				switch(e.message) {
				case error.known.NEED_ADMIN_RIGHTS_IN_CHANNEL:
					await User.resetTickerMessageId(
						{ workerName: _self.config.WORKER_NAME, chatId })
					Sentry.captureMessage(
						formatError('Bot need admin rights to operate in this ' +
							'chat (tickerMessageId is reset).'))
					break
				default:
					Sentry.captureException(
						new TBTelegrafInternalError('Telegraf internal error.', e)
					)
				}
			})

			// You must update and cache tickers
			// before receiving any Telegram updates.
			debug('Initial tickers update.')
			await _self.updateTickers()

			// Delete webhook to avoid conflicts
			// with long-polling mode
			if(ENVIRONMENT !== 'development') {
				debug('Delete WebHook.')
				await _self._telegram.deleteWebhook()
			}

			if(_self._webhookServer) {
				// Run worker in WebHook mode
				debug('Launch in WebHook mode.')
				const webhookSecretPath =
					`/${crypto.randomBytes(32).toString('hex')}` +
					`/${_self.config.WORKER_NAME}` +
					`/${ENVIRONMENT}`

				const webHookUrl =
					`https://${SERVER_ADDRESS}` +
					`${WEBHOOK_SERVER_PORT !== 443 ? `:${WEBHOOK_SERVER_PORT}` : ''}` +
					`${webhookSecretPath}`

				// Restrict webhook access to all except
				// for Telegram servers and itself
				const whitelistIps = [ '149.154.160.0/20', '91.108.4.0/22' ]

				try {
					whitelistIps.push(dnsSync.resolve(`${SERVER_ADDRESS}`))
				} catch(e) {
					Sentry.captureException(new TBResolveServerIpError(
						`Failed to resolve IP of the server: "${SERVER_ADDRESS}".`))
				}

				debug(`Restrict WebHook access to IPs: ${whitelistIps.join(', ')}.`)

				_self._webhookServer.use(
					IpFilter(whitelistIps, {
						mode: 'allow', log: false, trustProxy: false,
						detectIp: (req) => {
							const address = req.connection.remoteAddress
							// Cut IPv6 styled IPv4 addresses
							if(address.indexOf('::ffff:') === 0) {
								return address.slice(7)
							}
							return address
						}
					}))

				_self._webhookServer.use(
					(err, req, res, next) => {
						if(err instanceof IpDeniedError) {
							res.writeHead(403, {'Content-Type': 'text/html'})
							res.statusMessage = 'Forbidden'
							res.end('<span style="font-weight: bold; font-size:200%;">' +
								'ERROR 403 &ndash; Forbidden</span>')
							return
						}
						return next()
					})

				// Add webhook routes to server
				_self._webhookServer.use(
					_self._telegraf.webhookCallback(webhookSecretPath))

				// Send keys to Telegram and set webhook
				debug(`Set WebHook to: ${webHookUrl}.`)
				await _self._telegram.setWebhook(webHookUrl, {
					source: path.resolve(__dirname, '../../server-cert.pem')
				})
			} else {
				// Run worker in Long-Polling mode
				debug('Launching in Long-Polling mode.')
				_self._telegraf.startPolling()
			}

			const updateTickersInterval =
				ENVIRONMENT === 'development' ?
					(60 * 1000 / 12) :
					_self.config.TICKER_UPDATE_INTERVAL

			setTimeout(async function update() {
				await _self.updateTickers()
				setTimeout(update, updateTickersInterval)
			}, updateTickersInterval)
		}
	}
}

module.exports = Worker
