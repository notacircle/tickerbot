
class _TBError extends Error {
	constructor(message){
		super(message)
		this.name = this.constructor.name
		this.message = message
		if (typeof Error.captureStackTrace === 'function'){
			Error.captureStackTrace(this, this.constructor)
		} else {
			this.stack = (new Error(message)).stack
		}
	}
}

class TBError extends _TBError {
	constructor(message, error) {
		const originalError = error
		if (!message || typeof message !== 'string') {
			throw new _TBError('TBError requires 1 arguments to be a string.')
		}

		if(!error) {
			super(message)
		} else {
			super(TBMessageFormatter([
				message,
				TBMessageFormatter({
					'Original error': originalError.message
				})
			]))
			this.original = originalError
			this.newStack = this.stack
			const message_lines = (this.message
				.match(/\n/g) || []).length + 1
			this.stack = this.stack.split('\n')
				.slice(0, message_lines + 1).join('\n') + '\n' +
				error.stack
		}
	}
}

function TBMessageFormatter(chunks) {
	if(new.target) {
		throw new TBError('TBMessageFormatter is not an constructor but function.')
	}

	if (!chunks || typeof chunks !== 'object') {
		throw new TBError('TBMessageFormatter requires 1 argument to be an object.')
	}

	const chunkMessages = []
	for (const [chunkName, chunkValue] of Object.entries(chunks)) {
		let chunkMessage = chunkValue
		if (chunkValue instanceof Object) {
			let propMessages = []
			for (const [propName, propValue] of Object.entries(chunkValue)) {
				propMessages.push(`${propName}::${propValue}`)
			}
			chunkMessage = propMessages.join(', ')
		}
		chunkMessages.push(
			Array.isArray(chunks) ?
				`${chunkMessage}` :
				`${chunkName}: ${chunkMessage}`
		)
	}
	return Array.isArray(chunks) ?
		chunkMessages.join(' | ') :
		chunkMessages.join('; ')
}

class TBGlobalError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBErrorOverflowError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBMessageCannotBeEditedError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBTelegrafInternalError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBResolveServerIpError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBFetchNomicsError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBFetchCbrError extends TBError {
	constructor(...args) {
		super(...args)
	}
}

class TBFetchMoexError extends TBError {
	constructor(...args) {
		super(...args)
	}
}


module.exports = {
	// List of known errors to handle
	known: require('./known'),

	// formatters
	TBMessageFormatter,

	// Errors
	TBError,

	// Custom errors
	TBGlobalError,
	TBErrorOverflowError,
	TBMessageCannotBeEditedError,
	TBTelegrafInternalError,
	TBResolveServerIpError,
	TBFetchNomicsError,
	TBFetchCbrError,
	TBFetchMoexError
}
