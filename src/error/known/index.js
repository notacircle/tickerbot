module.exports = {
	MESSAGE_IS_NOT_MODIFIED: '400: Bad Request: message is not modified: specified new message content and reply markup are exactly the same as a current content and reply markup of the message',
	MESSAGE_TO_EDIT_NOT_FOUND: '400: Bad Request: message to edit not found',
	BOT_IS_NOT_A_MEMBER_OF_SUPERGROUP: '403: Forbidden: bot is not a member of the supergroup chat',
	BOT_WAS_KICKED_FROM_THE_GROUP_CHAT: '403: Forbidden: bot was kicked from the group chat',
	CHAT_NOT_FOUND: '400: Bad Request: chat not found',
	MESSAGE_IS_INVALID: '400: Bad Request: MESSAGE_ID_INVALID',
	BOT_IS_NOT_A_MEMBER_OF_CHANNEL: '403: Forbidden: bot is not a member of the channel chat',
	NEED_ADMIN_RIGHTS_IN_CHANNEL: '400: Bad Request: need administrator rights in the channel chat',
	MESSAGE_CANT_BE_EDITED: '400: Bad Request: message can\'t be edited'
}
