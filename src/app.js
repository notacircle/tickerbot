require('dotenv').config()
require('module-alias/register')

const {
	ENVIRONMENT,
	SENTRY_DSN
} = process.env

const {
	TBGlobalError
} = require('./error')
const workers = require('./workers')
const { connect } = require('./models')

const _debug = require('debug')
const debug = _debug('app')
const error = _debug('error')

const fs = require('fs')
const path = require('path')
const https = require('https')
const express = require('express')
const Sentry = require('@sentry/node')
const Integrations = require('@sentry/integrations')

debug(`Running environment: "${ENVIRONMENT}".`)

// Sentry error-tracking system init
if(SENTRY_DSN) debug(`Setup Sentry for DSN: ${SENTRY_DSN}.`)
else debug('Sentry DSN is not specified!')

Sentry.init({
	enabled: !!SENTRY_DSN, // enable if SENTRY_DSN exists
	dsn: SENTRY_DSN,
	environment: ENVIRONMENT,
	beforeSend: (event, hint) => {
		// Catch error and print in console if env is 'development'
		if (`${ENVIRONMENT}` === 'development') {
			error(hint.originalException
				|| hint.syntheticException || event)
			return null // this drops the event and nothing will be send to sentry
		}
		return event
	},
	integrations: [
		// Prevent Sentry limits exceeded
		new Integrations.Dedupe(),
		new Integrations.ExtraErrorData()
	]
})

;(async function app() {
	try {
		// Connecting Database
		debug('Connect database.')
		await connect()

		// Setup WebHook Server
		let webhookServer = null
		if (`${ENVIRONMENT}` === 'staging' || `${ENVIRONMENT}` === 'production') {
			debug('Starting WebHook server.')
			webhookServer = express()
			https.createServer({
				key: fs.readFileSync(
					path.resolve(__dirname, '../server-key.pem')),
				cert: fs.readFileSync(
					path.resolve(__dirname, '../server-cert.pem'))
			}, webhookServer).listen(8443)
		}

		// Running Workers
		debug('Launch workers.')
		for (const _worker of workers) {
			await (new _worker(webhookServer)).launch()
		}
	} catch(e) {
		Sentry.captureException(
			new TBGlobalError('Fatal! Global error.', e))

		debug('Restarting in 15s.')
		setTimeout(app, 15 * 1000)
	}
})()
