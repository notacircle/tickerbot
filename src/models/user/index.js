const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UserSchema = new Schema({
	workerName: { type: String, required: true },
	chatId: { type: Number, required: true },
	tickerMessageId: { type: Number, required: false },
	chatType: { type: String, default: 'unknown', required: true },
	title: { type: String },
	username: { type: String },
	firstName: { type: String },
	lastName: { type: String }
}, {
	timestamps: {
		createdAt: 'created_at',
		updatedAt: 'updated_at'
	}
})

UserSchema.statics = {
	resetTickerMessageId: async(conditions) => {
		const user = await User.findOne(conditions)
		if(user) {
			user.tickerMessageId = null
			await user.save()
		}
	},
	findOneAndUpsert:  async (conditions, update) => {
		const user = await User.findOne(conditions)

		if(!user) {
			const newUser = new User({...update, ...conditions})
			await newUser.save()
		} else {
			Object.assign(user, update)
			await user.save()
		}

		// returning previous state
		return {
			isNew: !user
		}
	}
}


const User = mongoose.model('User', UserSchema)

module.exports = User


