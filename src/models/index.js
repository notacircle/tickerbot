const {
	MONGO_CONNECTION_STRING
} = process.env

const User = require('./user')
const mongoose = require('mongoose')

async function connect() {
	await mongoose.connect(MONGO_CONNECTION_STRING, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false
	})
	mongoose.connection.on('error', e => {throw e})
}

module.exports = {
	connect,
	User
}
