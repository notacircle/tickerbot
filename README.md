## Description
TickerBot is a set of **Telegram** widgets (pinned bots)
to check some financial tickers. Pin it at the top of your 
chat list – wow its looking like a widget now!

### BTC/USD
https://t.me/Bitcoin_TickerBot

![BTC](src/res/bitcoin_tip.jpg)

### ETH/USD
https://t.me/Ethereum_TickerBot

![ETH](src/res/ethereum_tip.jpg)

### Market Cap
https://t.me/MarketCap_TickerBot

![M_CAP](src/res/marketcap_tip.jpg)

### RUB/USD
https://t.me/Ruble_TickerBot

![RUB](src/res/ruble_tip.jpg)

## Installation

Clone this repository.

Make .env file in the project root and change environment 
variables on yours.
```shell script
# environment name
ENVIRONMENT=development

# server configuration
SERVER_ADDRESS=
SSH_USER=
WEBHOOK_SERVER_PORT=
MONGO_CONNECTION_STRING=

# API keys
BITCOIN_TELEGRAM_TOKEN=
ETHEREUM_TELEGRAM_TOKEN=
MARKETCAP_TELEGRAM_TOKEN=
RUBLE_TELEGRAM_TOKEN=
NOMICS_API_KEY=
SENTRY_KEY=
MIXPANEL_TOKEN=
```

Install all dependencies.
```shell script
npm install --dev
```
Use this command to start. "nodemon" tool will watch any 
changes in the project ./src directory and will restart 
after making the changes.
```shell script
nodemon
```
Also you can use pure node to start.
```shell script
node ./src/app.js
```
