#!/bin/sh

# change hostname for local runs in docker
if [ "$ENVIRONMENT" == "development" ]; then
  HOST_IP=$(ip route show | awk '/default/ {print $3}')
  echo $HOST_IP
  MONGO_CONNECTION_STRING=${MONGO_CONNECTION_STRING/localhost/$HOST_IP}
  echo "New MONGO_CONNECTION_STRING:"
  echo $MONGO_CONNECTION_STRING
fi

# generate ssl cert for telegram webhook
echo "Generating SSL cert for WebHook..."
apk add --no-cache openssl
openssl req -newkey rsa:2048 -sha256 \
  -nodes -keyout ./server-key.pem -x509 \
  -days 3652 -out ./server-cert.pem \
  -subj "/C=DE/ST=Berlin/L=Berlin/O=TickerBot/CN=$SERVER_ADDRESS"
apk del openssl

exec "$@"
